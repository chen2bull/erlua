# 一般都是由第三方库自己提供<PackageName>Config.cmake 所以<PackageName>Config.cmake会和代码库目录有相对关系
# 但是Erlang没有提供这些，以下三行没有意义
if(NOT Erlang_ROOT)
    set(Erlang_ROOT "$ENV{Erlang_ROOT}")
endif()


# 让find_path和find_library 以这里为搜索起点
set(CMAKE_FIND_ROOT_PATH /usr/lib/erlang/)
find_path(Erlang_INCLUDE_DIRS REQUIRED
        NAMES ei.h
        NO_PACKAGE_ROOT_PATH  # 这个配置不在erlang目录中
        ONLY_CMAKE_FIND_ROOT_PATH
)
#message(Erlang_INCLUDE_DIRS:${Erlang_INCLUDE_DIRS})

find_library(Erlang_LIB_EI REQUIRED
        NAMES ei
        NO_PACKAGE_ROOT_PATH
        ONLY_CMAKE_FIND_ROOT_PATH
)
find_library(Erlang_LIB_ERL_INTERFACE REQUIRED
        NAMES erl_interface
        NO_PACKAGE_ROOT_PATH
        ONLY_CMAKE_FIND_ROOT_PATH
        )
find_library(Erlang_LIB_ERTS REQUIRED
        NAMES erts
        NO_PACKAGE_ROOT_PATH
        ONLY_CMAKE_FIND_ROOT_PATH
        )

list(APPEND Erlang_LIBRARIES ${Erlang_LIB_EI})
list(APPEND Erlang_LIBRARIES ${Erlang_LIB_ERL_INTERFACE})
list(APPEND Erlang_LIBRARIES ${Erlang_LIB_ERTS})
