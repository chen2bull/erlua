# TODO
- [x] 将参数中的 erlang tuple，解析为lua table
- [x] 将返回的lua table转换成erlang tuple
- [x] 加载文件
- [x] 执行特定方法
- [ ] 创建虚拟机的时候,改用erl_nif_alloc申请内存
- [ ] 统计内存的时候,用正确的参数
- [ ] 正确处理大整数,用int64

- [ ] 在lua中，输出日志到 error_logger
- [ ] 提供一个元表,确保所有lua虚拟机公用一份配置表,可多线程读取,禁止修改
- [ ] 在全局的地方，加载配置，并提供热更新机制
