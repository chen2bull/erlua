%%%-------------------------------------------------------------------
%%% @author cmj
%%% @copyright (C) 2020, <COMPANY>
%%% @doc
%%% @end
%%%-------------------------------------------------------------------
-module(raw_server).

-behaviour(gen_server).

-export([
  start_link/0,
  join_in/0,
  move_to/2
]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-record(raw_server_state, {server_ls = []}).

%%%===================================================================
%%% Spawning and gen_server implementation
%%%===================================================================

start_link() ->
  gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%% 加入房间
join_in() ->
  gen_server:call(?SERVER, {join_in, erlang:self()}).

move_to(X, Y) ->
  gen_server:send(?SERVER, {move_to, X, Y}).

init([]) ->
  {ok, #raw_server_state{}}.

handle_call({join_in, Pid}, _From, State = #raw_server_state{}) ->

  {reply, ok, State};
handle_call(_Request, _From, State = #raw_server_state{}) ->
  {reply, ok, State}.

handle_cast(_Request, State = #raw_server_state{}) ->
  {noreply, State}.

handle_info(_Info, State = #raw_server_state{}) ->
  {noreply, State}.

terminate(_Reason, _State = #raw_server_state{}) ->
  ok.

code_change(_OldVsn, State = #raw_server_state{}, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
