%%%-------------------------------------------------------------------
%%% @author cmj
%%% @copyright (C) 2020, <COMPANY>
%%% @doc
%%% @end
%%%-------------------------------------------------------------------
-module(lua_server).

-behaviour(gen_server).

-export([start_link/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-record(lua_server_state, {lua_state}).

%%%===================================================================
%%% Spawning and gen_server implementation
%%%===================================================================

start_link() ->
  gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

init([]) ->
  LuaState = erlua:newstate(),
  {ok, #lua_server_state{lua_state = LuaState}}.

handle_call(_Request, _From, State = #lua_server_state{}) ->
  {reply, ok, State}.

handle_cast(_Request, State = #lua_server_state{}) ->
  {noreply, State}.

handle_info(_Info, State = #lua_server_state{}) ->
  {noreply, State}.

terminate(_Reason, _State = #lua_server_state{}) ->
  ok.

code_change(_OldVsn, State = #lua_server_state{}, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
