-module(client).

-behaviour(gen_server).

-export([start_link/1]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-define(SERVER, ?MODULE).

-record(client_state, {}).

start_link(N) when is_integer(N) ->
  Name = role_server_name(N),
  gen_server:start_link({local, Name}, ?MODULE, [], []).

%%%===================================================================
%%% Spawning and gen_server implementation
%%%===================================================================

init([]) ->
  {ok, #client_state{}}.

handle_call(_Request, _From, State = #client_state{}) ->
  {reply, ok, State}.

handle_cast(_Request, State = #client_state{}) ->
  {noreply, State}.

handle_info(_Info, State = #client_state{}) ->
  {noreply, State}.

terminate(_Reason, _State = #client_state{}) ->
  ok.

code_change(_OldVsn, State = #client_state{}, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
role_server_name(N) ->
  NameStr = "client_" ++ erlang:integer_to_list(N),
  erlang:list_to_atom(NameStr).
