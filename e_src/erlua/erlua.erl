-module(erlua).
-author("cmj").

%% API
-export([
    newstate/0,
    load_luafile/2,
    memory_use/1,
    gc/1,
    func_ls/3,
    func/2,
    func/3,
    func/4,
    func/5,
    func/6,
    func/7,
    func/8,
    func/9,
    func/10,
    func/11,
    func/12,
    func/13,
    func/14,
    func/15,
    func/16,
    func/17,
    func/18,
    func/19
]).
-on_load(init/0).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-spec(newstate() ->
    {ok, LuaState :: term()} |
    {exception, Reason :: term()} |
    {error, Reason :: term()}).
newstate() ->
    erlang:throw(nif_library_not_loaded).

load_luafile(_LuaStateRef, _) ->
    erlang:throw(nif_library_not_loaded).

-spec(gc(LuaState :: term()) ->
    ok |
    {error, Reason :: term()}).
gc(_) ->
    erlang:throw(nif_library_not_loaded).

-spec(memory_use(LuaState :: term()) ->
    {ok, Bytes :: integer()} |
    {error, Reason :: term()}).
memory_use(_) ->
    erlang:throw(nif_library_not_loaded).
func(_, _) ->
    erlang:throw(nif_library_not_loaded).
func(_, _, _) ->
    erlang:throw(nif_library_not_loaded).
func(_, _, _, _) ->
    erlang:throw(nif_library_not_loaded).
func(_, _, _, _, _) ->
    erlang:throw(nif_library_not_loaded).
func(_, _, _, _, _, _) ->
    erlang:throw(nif_library_not_loaded).
func(_, _, _, _, _, _, _) ->
    erlang:throw(nif_library_not_loaded).
func(_, _, _, _, _, _, _, _) ->
    erlang:throw(nif_library_not_loaded).
func(_, _, _, _, _, _, _, _, _) ->
    erlang:throw(nif_library_not_loaded).
func(_, _, _, _, _, _, _, _, _, _) ->
    erlang:throw(nif_library_not_loaded).
func(_, _, _, _, _, _, _, _, _, _, _) ->
    erlang:throw(nif_library_not_loaded).
func(_, _, _, _, _, _, _, _, _, _, _, _) ->
    erlang:throw(nif_library_not_loaded).
func(_, _, _, _, _, _, _, _, _, _, _, _, _) ->
    erlang:throw(nif_library_not_loaded).
func(_, _, _, _, _, _, _, _, _, _, _, _, _, _) ->
    erlang:throw(nif_library_not_loaded).
func(_, _, _, _, _, _, _, _, _, _, _, _, _, _, _) ->
    erlang:throw(nif_library_not_loaded).
func(_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _) ->
    erlang:throw(nif_library_not_loaded).
func(_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _) ->
    erlang:throw(nif_library_not_loaded).
func(_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _) ->
    erlang:throw(nif_library_not_loaded).
func(_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _) ->
    erlang:throw(nif_library_not_loaded).
func_ls(_, _, _) ->
    erlang:throw(nif_library_not_loaded).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% local functions:
-define(ERLUA_VERSION, 0).

init() ->
    FilePath = filename:dirname(code:which(?MODULE)),
    io:format("MODULE:~str~n", [?MODULE]),
    io:format("FilePath:~str~n", [FilePath]),
    io:format("filename:absname(FilePath):~str~n", [filename:absname(FilePath)]),
    FileFullName = filename:join(filename:absname(FilePath), ?MODULE),
    io:format("start load:~str~n", [FileFullName]),
    ok = erlang:load_nif(FileFullName, ?ERLUA_VERSION).
