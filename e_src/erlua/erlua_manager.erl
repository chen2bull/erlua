%%%-------------------------------------------------------------------
%%% @author cmj
%%% @copyright (C) 2020, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 21. 7月 2020 下午12:01
%%%-------------------------------------------------------------------
-module(erlua_manager).
-author("cmj").

%% API
-export([
    force_upgrade_module/0
]).

%% 热更nif的C代码库
force_upgrade_module() ->
    force_upgrade_module(erlua).

%% 注意:谨慎使用nif的热更机制,现在erlang的nif热更机制实现仍然有BUG:
%% 1.需要调用两次 purge 和 delete,然后再执行code:load_file才能热更动态库,
%% 2.而且热更的时候,ERL_NIF_INIT的load方法会被重复执行,而不是第一次加载执行load,第二次加载执行upgrade)
%% 更新C代码流程:
%% 1. 修改C实现的nif
%% 2. 编译新代码成so文件
%% 3. 执行 force_upgrade_module 方法,即可热更C++代码
force_upgrade_module(Mod) ->
    code:purge(Mod),
    code:delete(Mod),
    code:purge(Mod),
    code:delete(Mod),
    {module, Mod} = code:load_file(Mod).
