# erlua

#### 介绍
erlang通过nilf调用lua

#### 编译说明
1. 安装cmake,gcc,gdb,lcov
2. 用系统自带的包管理工具安装lua5.3和erlang
3. 在代码目录下创建一个"编译目录",并产生编译target
```
cd ~/code/erlua/    # 代码目录
mkdir cmake-build-debug/
cd cmake-build-debug/
cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_MAKE_PROGRAM=make -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -G "CodeBlocks - Unix Makefiles" ~/codes/erlua/
```
4. 编译
```
cmake --build ./ --target clean -- -j 6
cmake --build ./ --target all -- -j 6
```

5.将erlua.so拷贝到和erlua.beam同一个目录下

#### 为防止出错要做的准备
1. 查看当前core_dump文件产生的目录
```
cat /proc/sys/kernel/core_pattern
```
2. 修改/etc/default/apport这个文件, 设置enabled=0

默认设置下会用 apport 产生crash report而不是core dump,必须先关闭 apport

3. 创建存放 coredump文件的目录
```
mkdir -p /data/coredump
chmod 777 /data/coredump/
```

4. 修改系统配置文件
```
vi /etc/sysctl.conf
```
添加以下内容
```
kernel.core_pattern = /data/coredump/core.%e.%p.%t
```

4. 修改core 文件大小,执行以下命令,并将复制粘贴到 /etc/profile 中
```
ulimit -c unlimited
```

5. 重启机器确保以上修改都生效
```bash
cmj@cmj-Ub20:~$ ulimit -c
unlimited
cmj@cmj-Ub20:~$ cat /proc/sys/kernel/core_pattern
/data/coredump/core.%e.%p.%t
```
完成以上设置以后,如果程序崩溃(几乎不可能),就产生core dump文件

#### 使用说明
1. 将编译产生的文件erlua.so放到 erlua.beam同一个目录下
2. 创建lua虚拟机,
```erlang
    {ok, State} = erlua:newstate(),

```
3. 加载lua代码文件
```erlang
{ok, Ret} = erlua:load_luafile(State, "./mod1.lua"),
```

4. 执行具体某一个方法
```erlang
68 = erlua:func_ls(State, add_func, [12, 56])
```
5. 自带的test和luascript目录下有一些例子
6. 建议通过 erlua_agent 访问lua虚拟机

